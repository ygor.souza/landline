use hidapi::DeviceInfo;
use iced::{
    button, scrollable, Button, Column, Container, Element, Length, Sandbox, Scrollable, Settings, Space,
    Text,
};

pub fn main() -> iced::Result {
    App::run(Settings::default())
}

pub struct App {
    hidapi: Box<hidapi::HidApi>,
    scroll: scrollable::State,
    refresh: button::State,
}

impl Sandbox for App {
    type Message = Message;
    fn new() -> App {
        App {
            hidapi: Box::new(hidapi::HidApi::new().unwrap()),
            scroll: scrollable::State::new(),
            refresh: button::State::new(),
        }
    }

    fn title(&self) -> String {
        "Landline".to_string()
    }

    fn update(&mut self, event: Message) {
        match event {
            Message::RefreshPressed => self.hidapi.refresh_devices(),
        };
    }

    fn view(&mut self) -> Element<Message> {
        let App { hidapi, scroll, refresh } = self;
        let mut controls = Column::new();
        controls = controls.push(Button::new(refresh, Text::new("Refresh")).on_press(Message::RefreshPressed));
        let devices = Scrollable::new(scroll);
        let devices = devices.push(hidapi.device_list().fold(
            Column::new().padding(5).spacing(10),
            |items, next| {
                items
                    .push(Text::new(format_device_info(next)).size(16))
                    .push(Space::new(Length::Units(0), Length::Units(10)))
            },
        ));
        controls = controls.push(devices).push(Space::with_width(Length::Fill));
        Container::new(controls).height(Length::Fill).into()
    }
}
fn format_device_info(info: &DeviceInfo) -> String {
    format!(
        "VID: 0x{:04x}
        \nPID: 0x{:04x}
        \nSN: {}
        \nRelease Number: {}
        \nProduct String: {}
        \nManufacturer String: {}
        \n",
        info.vendor_id(),
        info.product_id(),
        info.serial_number().unwrap_or("None"),
        format_release_number(info.release_number()),
        info.product_string().unwrap_or("None"),
        info.manufacturer_string().unwrap_or("None"),
    )
}
fn format_release_number(release_number: u16) -> String {
    format!("{:x}.{:02x}", release_number >> 8, release_number & 0xff)
}

#[derive(Debug, Clone)]
pub enum Message {
    RefreshPressed,
}



